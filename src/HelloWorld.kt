import java.awt.SystemColor.text

//fun main (args : Array<String>){
//    val x:Int = 1
//    val y:Int = 2
//    val z= x+y
//    println("Hello,World! $z")
//
//    // NUllable
//    var data: Int
//    var dataNulable: Int?
//    data = 2
//    println( y == data)
//    println( y == data)
//    dataNulable = null
//    data = dataNulable
//}

fun main(args: Array<String>) {
    showName(surname = "Sanchai")
    showName(name = "Prayuth")

    gradeReport("Prayuth" , "chun O cha",0.25)
    gradeReport( name = "Prayuth")
    gradeReport( surname = "chun O cha")

    gradeReport( gpa = 0.25)
    println(isOdd(30))
    println(isOdd(31))

    println( getAbbreviation('A'))
    println( getAbbreviation('B'))
    println( getAbbreviation('F'))
    println( getAbbreviation('C'))
    println( getAbbreviation('D'))

    println( getGrade(60))
    println( getGrade(75))


    println( hiGrade(75))

    for( i in 1..3){
        println(i)
    }

    for( i in 6 downTo 0 step 2){
        println(i)
    }

//    var arrays = arrayOf(1,3,5,7)
//    for (i in arrays.indices){
//        println(arrays[i])
//    }

//    for((index,value) in arrays.withIndex()){
//        println("$index. value = $value")
//    }

    var arrays = arrayOf(5,55,200,1,3,5,7)
    var max = findMaxValue(arrays)
    println("max value is $max")



}

fun gradeReport (name: String = "default ",surname : String = "default " , gpa: Double = 0.00): Unit {
    println ("mister $name $surname gpa: is $gpa")
}

fun showName(surname: String = "default ", name: String = "default "): Unit {
    println("surname : $surname  name : $name")
}

fun isOdd(value: Int): String {
    if (value.rem(2) == 0) {
        return "$value is even value"
    } else {
        return "$value is even odd"
    }
}

fun getAbbreviation(abbr: Char): String {
    when (abbr) {
        'A' -> return "Abnormal"
        'B' -> return "Bad boy"
        'F' -> return "Fantastic"
        'C' -> {
            println("not smart")
            return "Cheap"
        }
        else -> return "Hello"
    }
}

fun getGrade(score: Int): String? {
    var grade:String?
    when (score) {
        in 0..50 -> grade = "F"
        in 51..70 -> grade = "C"
        in 70..80 -> grade = "B"
        in 80..100 -> grade = "A"
        else -> grade = null
    }
    return grade
}

fun hiGrade(score: Int): String? {
    val grade = getGrade(score)
    val result : String?
    when (grade) {
        null -> result = "not have grade yet"
        else -> result = "score $score  ="+getAbbreviation(grade.toCharArray()[0])
    }
    return  result
}

fun findMaxValue(values: Array<Int>): Int {
    var max : Int = values[0]
    for( i  in values){
        if( i > max){
            max = i
        }
    }
    return max

}








