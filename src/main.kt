//class Person(var name: String, var surname: String, var gpa: Double ) {
//    constructor (name:String , surname : String) : this(name,surname,0.0){
//
//    }
//
//    fun getDetails(): String {
//        return "$name $surname has soore $gpa "
//    }
//}
//
//
//fun main(args: Array<String>) {
//    val no1: Person = Person("Prayut", "Jun o Cha", 0.25 )
//    val no2: Person = Person("kim", "eiei" )
//    val no3: Person = Person(name = "Prayut", surname ="Jun o Cha")
//
//    val no4: Person  = Person (name = "Prayut", surname ="Jun o Cha" , gpa=4.00)
//    println(no1.getDetails())
//    println(no2.getDetails())
//    println(no3.getDetails())
//    println(no4.getDetails())
//
//}

//nol result Person@61bbe9ba
//nol.name result Prayut


//open class Person(var name: String, var surname: String, var gpa: Double) {
//    constructor(name: String, surname: String) : this(name, surname, 0.0) {
//
//    }
//
//    open fun getDetails(): String {
//        return "$name $surname has soore $gpa "
//    }
//}
//
//class Student(name: String, surname: String, gpa: Double, var department: String) : Person(name, surname, gpa) {
//    override fun getDetails(): String {
////        return "$name $surname has soore $gpa and study in $department"
//        return super.getDetails() + "and study in $department"
//    }
//}
//
//fun main(args: Array<String>) {
//    val no1: Student = Student("Prayut", "tuu", 2.00, "government")
//    println(no1.getDetails())
//}

abstract class Person(var name: String, var surname: String, var gpa: Double) {
    constructor(name: String, surname: String) : this(name, surname, 0.0) {

    }
    abstract fun goodBoy(): Boolean
    open fun getDetails(): String {
        return "$name $surname has soore $gpa "
    }
}

class Student(name: String, surname: String, gpa: Double, var department: String) : Person(name, surname, gpa) {
    override fun goodBoy(): Boolean {
        return gpa > 2.00
    }

    override fun getDetails(): String {
//        return "$name $surname has soore $gpa and study in $department"
        return super.getDetails() + "and study in $department"
    }
}

fun main(args: Array<String>) {
    val no1: Student = Student("Prayut", "tuu", 2.00, "government")
    println(no1.getDetails())
}




