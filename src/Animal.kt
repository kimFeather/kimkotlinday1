abstract class Animal (var leg: Int, var food: String){

     abstract fun getSound(): String
//     {
//        return " have $leg legs , like to eat $food "
//    }
}

class Dog (leg : Int, food: String, var name: String) : Animal (leg , food){

    override fun getSound(): String {
        return "name is  $name sound HONG HONG HONG have $leg leg eat $food"
    }
}

class Lion (leg : Int, food: String) : Animal (leg , food){
    override fun getSound(): String {
        return "Singto sound MEAW MEAW MEAW have $leg leg eat $food"
    }

}

fun main(args: Array<String>) {
    val dog: Dog = Dog(4, "tiger", "dog")
    val lion: Lion = Lion(5, "fish")
    println(dog.getSound())
    println(lion.getSound())
}